
@extends('layouts.layout')

@section('content')
@php
    $routes = App\Route::all();
@endphp
            <!-- MAIN CONTENT-->
            <div class="main-content">
                <div class="section__content section__content--p30">
                    <div class="container-fluid">
                        <div class="row">


                            <div class="col-lg-6">
                                <div class="card">
                                    <form method="POST" action="/clients/{{$client->id}}">
                                        @csrf
                                        @method('PUT')
                                        <div class="card-header">
                                            <strong style="color: red; text-align:center;">La editarea datelor clientilor se completeaza toate campurile!</strong><small></small>
                                        </div>

                                    <div class="card-header">
                                        <strong>Editare client</strong>
                                        <small></small>
                                    </div>
                                    <div class="card-body card-block">
                                        <div class="form-group">
                                            <label for="name" class=" form-control-label">Denumire</label>
                                        <input type="text" id="name" name="name" placeholder="adauga denumire" class="form-control" selected="true" value="{{$client->name}}">
                                        </div>
                                        <div class="form-group">
                                            <label for="street" class=" form-control-label">Strada</label>
                                            <input type="text" id="street"  name="street" placeholder="adauga strada si nr. strada" class="form-control" value = {{$client->street}}>
                                        </div>
                                        <div class="form-group">
                                            <label for="city" class=" form-control-label">Localitate</label>
                                            <input type="text" id="city" name="city" placeholder="adauga localitate" class="form-control" value = {{$client->city}}>
                                        </div>

                                        <div class="form-group">
                                            <label for="county" class=" form-control-label">Judet</label>
                                            <input type="text" id="county" name="county" placeholder="adauga judet" class="form-control" value = {{$client->county}}>
                                        </div>
                                        <div class="form-group">
                                            <label for="cui" class=" form-control-label">C.U.I.</label>
                                            <input type="text" id="cui" name="cui" placeholder="adauga C.U.I." class="form-control" value = {{$client->cui}}>
                                        </div>



                                        <div class="row form-group">
                                            <div class="col col-md-3">
                                                <label class=" form-control-label">Ziua colectarii</label>
                                            </div>
                                            <div class="col col-md-9">
                                                <div class="form-check-inline form-check">
                                                    <label class="form-check-label ">
                                                        <input type="checkbox" id="pickup_days" name="pickup_days[]" value="Monday" class="form-check-input" {{  ($client->pickup_days == "Monday" ? ' checked' : '') }}>Luni
                                                    </label>
                                                    <label class="form-check-label ">
                                                        <input type="checkbox" id="pickup_days" name="pickup_days[]" value="Tuesday" class="form-check-input" {{  ($client->pickup_days == "Tuesday" ? ' checked' : '') }}>Marti
                                                    </label>
                                                    <label class="form-check-label ">
                                                        <input type="checkbox" id="pickup_days" name="pickup_days[]" value="Wednesday" class="form-check-input" {{  ($client->pickup_days == "Wednesday" ? ' checked' : '') }}>Miercuri
                                                    </label>
                                                    <label class="form-check-label ">
                                                        <input type="checkbox" id="pickup_days" name="pickup_days[]" value="Thursday" class="form-check-input" {{  ($client->pickup_days == "Thursday" ? ' checked' : '') }}>Joi
                                                    </label>
                                                    <label class="form-check-label ">
                                                        <input type="checkbox" id="pickup_days" name="pickup_days[]" value="Friday" class="form-check-input" {{  ($client->pickup_days == "Friday" ? ' checked' : '') }}>Vineri
                                                    </label>
                                                </div>



                                            </div>
                                        </div>



                                        <div class="row form-group">
                                            <div class="col col-md-3">
                                                <label for="selectSm" class=" form-control-label">Ruta</label>
                                            </div>
                                            <div class="col-12 col-md-9">
                                                <select name="route_id" id="route_id" class="form-control-sm form-control" selected>
                                                    @foreach ($routes as $route)
                                                        <option value="{{$route->id}}">{{$route->number}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="order" class=" form-control-label">Numar ordine.</label>
                                            <input type="number" id="order" name="order" placeholder="adauga numar ordine" class="form-control" selected = "true">
                                        </div>



                                            <div class="col col-md-9">
                                                <div class="col col-md-3">
                                                    <label class=" form-control-label"><strong>Alege saptamana</strong></label>
                                                </div>
                                                <div class="form-check-inline form-check">
                                                    <label class="form-check-label ">
                                                        <input type="checkbox" id="collections_rithm" name="collections_rithm[]" value=1 class="form-check-input" selected oninvalid="alert('Trebuie sa completezi toate campurile!');" required>1
                                                    </label>
                                                </div>
                                                <div class="form-check-inline form-check">
                                                    <label class="form-check-label ">
                                                        <input type="checkbox" id="collections_rithm" name="collections_rithm[]" value=2 class="form-check-input" selected oninvalid="alert('Trebuie sa completezi toate campurile!');" required>2
                                                    </label>
                                                </div>
                                                <div class="form-check-inline form-check">
                                                    <label class="form-check-label ">
                                                        <input type="checkbox" id="collections_rithm" name="collections_rithm[]" value=3 class="form-check-input" selected oninvalid="alert('Trebuie sa completezi toate campurile!');" required>3
                                                    </label>
                                                </div>
                                                <div class="form-check-inline form-check">
                                                    <label class="form-check-label ">
                                                        <input type="checkbox" id="collections_rithm" name="collections_rithm[]" value=4 class="form-check-input" selected oninvalid="alert('Trebuie sa completezi toate campurile!');" required>4
                                                    </label>
                                                </div>
                                                <div class="form-check-inline form-check">
                                                    <label class="form-check-label ">
                                                        <input type="checkbox" id="collections_rithm" name="collections_rithm[]" value=5 class="form-check-input" selected oninvalid="alert('Trebuie sa completezi toate campurile!');" required>5
                                                    </label>

                                                </div>


                                            </div>






                                            <div class="col col-md-9">
                                                <div class="col col-md-3">
                                                    <label class=" form-control-label"><strong>Alege cod deseuri</strong></label>
                                                </div>
                                                <div class="form-check-inline form-check">
                                                    <label class="form-check-label ">
                                                        <input type="checkbox" id="toxic_waste_code" name="toxic_waste_code[]" value=1801 class="form-check-input" selected oninvalid="alert('Trebuie sa completezi toate campurile!');" required>18 01
                                                    </label>
                                                </div>
                                                <div class="form-check-inline form-check">
                                                    <label class="form-check-label ">
                                                        <input type="checkbox" id="toxic_waste_code" name="toxic_waste_code[]" value=180101 class="form-check-input" selected oninvalid="alert('Trebuie sa completezi toate campurile!');" required>18 01 01
                                                    </label>
                                                </div>
                                                <div class="form-check-inline form-check">
                                                    <label class="form-check-label ">
                                                        <input type="checkbox" id="toxic_waste_code" name="toxic_waste_code[]" value=180102 class="form-check-input" selected oninvalid="alert('Trebuie sa completezi toate campurile!');" required>18 01 02
                                                    </label>
                                                </div>
                                                <div class="form-check-inline form-check">
                                                    <label class="form-check-label ">
                                                        <input type="checkbox" id="toxic_waste_code" name="toxic_waste_code[]" value=180103* class="form-check-input" selected oninvalid="alert('Trebuie sa completezi toate campurile!');" required>18 01 03*
                                                    </label>
                                                </div>
                                                <div class="form-check-inline form-check">
                                                    <label class="form-check-label ">
                                                        <input type="checkbox" id="toxic_waste_code" name="toxic_waste_code[]" value=180104 class="form-check-input" selected oninvalid="alert('Trebuie sa completezi toate campurile!');" required>18 01 04
                                                    </label>

                                                </div>
                                                <div class="form-check-inline form-check">
                                                    <label class="form-check-label ">
                                                        <input type="checkbox" id="toxic_waste_code_" name="toxic_waste_code[]" value=180106* class="form-check-input" selected oninvalid="alert('Trebuie sa completezi toate campurile!');" required">18 01 06*
                                                    </label>

                                                </div>
                                                <div class="form-check-inline form-check">
                                                    <label class="form-check-label ">
                                                        <input type="checkbox" id="toxic_waste_code" name="toxic_waste_code[]" value=180107 class="form-check-input" selected oninvalid="alert('Trebuie sa completezi toate campurile!');" required>18 01 07
                                                    </label>

                                                </div>
                                                <div class="form-check-inline form-check">
                                                    <label class="form-check-label ">
                                                        <input type="checkbox" id="toxic_waste_code" name="toxic_waste_code[]" value=180108* class="form-check-input" selected oninvalid="alert('Trebuie sa completezi toate campurile!');" required>18 01 08*
                                                    </label>

                                                </div>
                                                <div class="form-check-inline form-check">
                                                    <label class="form-check-label ">
                                                        <input type="checkbox" id="toxic_waste_code" name="toxic_waste_code[]" value=180109 class="form-check-input" selected oninvalid="alert('Trebuie sa completezi toate campurile!');" required>18 01 09
                                                    </label>

                                                </div>
                                                <div class="form-check-inline form-check">
                                                    <label class="form-check-label ">
                                                        <input type="checkbox" id="toxic_waste_code_" name="toxic_waste_code[]" value=180110* class="form-check-input" selected oninvalid="alert('Trebuie sa completezi toate campurile!');" required>18 01 10*
                                                    </label>

                                                </div>
                                                <div class="form-check-inline form-check">
                                                    <label class="form-check-label ">
                                                        <input type="checkbox" id="toxic_waste_code" name="toxic_waste_code[]" value=1802 class="form-check-input" selected oninvalid="alert('Trebuie sa completezi toate campurile!');" required>18 02
                                                    </label>

                                                </div>
                                                <div class="form-check-inline form-check">
                                                    <label class="form-check-label ">
                                                        <input type="checkbox" id="toxic_waste_code" name="toxic_waste_code[]" value=180201 class="form-check-input" selected oninvalid="alert('Trebuie sa completezi toate campurile!');" required>18 02 01
                                                    </label>

                                                </div>
                                                <div class="form-check-inline form-check">
                                                    <label class="form-check-label ">
                                                        <input type="checkbox" id="toxic_waste_code" name="toxic_waste_code[]" value=180202* class="form-check-input" selected oninvalid="alert('Trebuie sa completezi toate campurile!');" required>18 02 02*
                                                    </label>

                                                </div>
                                                <div class="form-check-inline form-check">
                                                    <label class="form-check-label ">
                                                        <input type="checkbox" id="toxic_waste_code" name="toxic_waste_code[]" value=180203 class="form-check-input" selected oninvalid="alert('Trebuie sa completezi toate campurile!');" required>18 02 03
                                                    </label>

                                                </div>

                                                <div class="form-check-inline form-check">
                                                    <label class="form-check-label ">
                                                        <input type="checkbox" id="toxic_waste_code" name="toxic_waste_code[]" value=180205* class="form-check-input" selected oninvalid="alert('Trebuie sa completezi toate campurile!');" required>18 02 05*
                                                    </label>

                                                </div>

                                                <div class="form-check-inline form-check">
                                                    <label class="form-check-label ">
                                                        <input type="checkbox" id="toxic_waste_code" name="toxic_waste_code[]" value=180206 class="form-check-input" selected oninvalid="alert('Trebuie sa completezi toate campurile!');" required>18 02 06
                                                    </label>

                                                </div>

                                                <div class="form-check-inline form-check">
                                                    <label class="form-check-label ">
                                                        <input type="checkbox" id="toxic_waste_code" name="toxic_waste_code[]" value=180207* class="form-check-input" selected oninvalid="alert('Trebuie sa completezi toate campurile!');" required>18 02 07*
                                                    </label>

                                                </div>

                                                <div class="form-check-inline form-check">
                                                    <label class="form-check-label ">
                                                        <input type="checkbox" id="toxic_waste_code" name="toxic_waste_code[]" value=180208 class="form-check-input" selected oninvalid="alert('Trebuie sa completezi toate campurile!');" required>18 02 08
                                                    </label>

                                                </div>


                                            </div>




                                        <div class="row form-group">
                                            <div class="col col-md-3">
                                                <label class=" form-control-label">Peste o tona</label>
                                            </div>
                                            <div class="col col-md-9">
                                                <select name="overtone" id="overtone" class="form-control" selected>
                                                    <option value="1">DA</option>
                                                    <option value="0">NU</option>
                                                </select>
                                            </div>
                                        </div>

                                        <button class="au-btn au-btn-icon au-btn--green au-btn--small">
                                            <i class="zmdi zmdi-plus"></i>editeaza client
                                        </button>

                                    </div>

                                </div>
                            </div>

                        </div>
                        @include('layouts.footer')
                    </div>
                </div>
            </div>


@endsection




