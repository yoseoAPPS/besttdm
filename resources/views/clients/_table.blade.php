<tr class="tr-shadow row1" data-id="{{ $client->id }}">

    <td>
        <label class="au-checkbox">
            <input type="checkbox">
            <span class="au-checkmark"></span>
        </label>
    </td>

     <td><a href="/clients/{{$client['id']}}">{{$client['name']}}</a> |{{$client->order}}|</td>

    <td class="desc">{{$client['city']}}</td>
    <td>{{$client['county']}}</td>

    
    <td>
        <div class="table-data-feature">

            <a href="/clients/{{$client['id']}}/edit">
                <button class="item" data-toggle="tooltip" data-placement="top" title="Edit">
                    <i class="zmdi zmdi-edit"></i>
                </button>
            </a>

            <form action="/clients/{{$client['id']}}" method="POST">
                @csrf
                 @method('DELETE')
            <button class="item" data-toggle="tooltip" data-placement="top" title="Delete">
                <i class="zmdi zmdi-delete"></i>
            </button>
            </form>
            <button class="item" data-toggle="tooltip" data-placement="top" title="More">
                <a href="/clients/{{$client['id']}}"><i class="zmdi zmdi-more"></i></a>
            </button>
        </div>
    </td>
</tr>
