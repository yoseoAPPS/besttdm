@extends('layouts.layout')

@section('content')


@php


    $routes = App\Route::with('clients')->get();
    $route = new App\Route;
    $week_of_month = \Illuminate\Support\Carbon::now()->weekOfMonth;
        $dt = \Illuminate\Support\Carbon::tomorrow();
        $tomorrow = '';


        if ($dt->isWeekend()) {
           $tomorrow = \Illuminate\Support\Carbon::tomorrow()->next('Monday')->format('l');
        } else {
            $tomorrow = \Illuminate\Support\Carbon::tomorrow()->format('l');
        }



@endphp

    <div class="main-content">
        <div class="section__content section__content--p30">
            <div class="container-fluid">
                <div class="row">
                </div>
                    <section class="p-t-20">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-12">
                                    <h3 class="title-5 m-b-35">Clienti</h3>

                                    <div class="table-data__tool">

                                        <div class="table-data__tool-right">
                                            <a href="/clients/create">
                                                <button class="au-btn au-btn-icon au-btn--green au-btn--small">
                                                <i class="zmdi zmdi-plus"></i>adauga clienti</button>
                                            </a>
                                            
                                        </div>
                                    </div>
                                    @foreach($routes as $route)
                                    <h3 style="text-align: center">Ruta numarul {{$route->number}}</h3>
                                    <div class="table-responsive table-responsive-data2">
                                        <table  class="table table-data2">
                                            <thead >
                                                <tr>
                                                    <th>
                                                        <label class="au-checkbox">
                                                            <input type="checkbox">
                                                            <span class="au-checkmark"></span>
                                                        </label>
                                                    </th>
                                                    <th>Nume |numar ordine|</th>
                                                    <th>Localitate</th>
                                                    <th>Judet</th>
                                                    <th>actiuni</th>
                                                </tr>
                                            </thead>
                                            <tbody class="tablecontents">


                                                    @foreach($route->clients()->whereJsonContains('pickup_days', [$tomorrow])
                                                        ->whereJsonContains('collections_rithm', [(string)$week_of_month])
                                                        ->orderBy('order', 'asc')
                                                        ->get();
                                                        as $client)


                                                        @include('clients._table')

                                                    @endforeach


                                            </tbody>
                                        </table>


                                    </div>
                                    @endforeach


                                </div>
                            </div>
                        </div>
                    </section>


                    @include('layouts.footer')
            </div>
        </div>
    </div>





@endsection
