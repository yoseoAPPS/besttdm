
@extends('layouts.layout')

@section('content')
            <!-- MAIN CONTENT-->
            @php
                $routes = App\Route::latest()->get();


            @endphp


            <div class="main-content">
                <div class="section__content section__content--p30">
                    <div class="container-fluid">
                        <div class="row">

                            <div class="col-lg-6">
                                <div class="card">
                                    <form method="POST" action="/clients">
                                        @csrf

                                    <div class="card-header">
                                        <strong>Adaugare client</strong>
                                        <small></small>
                                    </div>
                                    @if ($errors->any())
                                        <div class="alert alert-danger">
                                            <ul>
                                                @foreach ($errors->all() as $error)
                                                    <li>{{ $error }}</li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    @endif

                                    <div class="card-body card-block">
                                        <div class="form-group">
                                            <label for="name" class=" form-control-label">Denumire</label>
                                            <input type="text" id="name" name="name" placeholder="adauga denumire" class="form-control">
                                        </div>
                                        <div class="form-group">
                                            <label for="street" class=" form-control-label">Strada</label>
                                            <input type="text" id="street"  name="street" placeholder="adauga strada si nr. strada" class="form-control">
                                        </div>
                                        <div class="form-group">
                                            <label for="city" class=" form-control-label">Localitate</label>
                                            <input type="text" id="city" name="city" placeholder="adauga localitate" class="form-control">
                                        </div>

                                        <div class="form-group">
                                            <label for="county" class=" form-control-label">Judet</label>
                                            <input type="text" id="county" name="county" placeholder="adauga judet" class="form-control">
                                        </div>
                                        <div class="form-group">
                                            <label for="cui" class=" form-control-label">C.U.I.</label>
                                            <input type="text" id="cui" name="cui" placeholder="adauga C.U.I." class="form-control">
                                        </div>



                                        <div class="row form-group">
                                            <div class="col col-md-3">
                                                <label class=" form-control-label">Ziua colectarii</label>
                                            </div>
                                            <div class="col col-md-9">
                                                <div class="form-check-inline form-check">
                                                    <label class="form-check-label ">
                                                        <input type="checkbox" id="pickup_days" name="pickup_days[]" value="Monday" class="form-check-input" >Luni
                                                    </label>
                                                    <label class="form-check-label ">
                                                        <input type="checkbox" id="pickup_days" name="pickup_days[]" value="Tuesday" class="form-check-input">Marti
                                                    </label>
                                                    <label class="form-check-label ">
                                                        <input type="checkbox" id="pickup_days" name="pickup_days[]" value="Wednesday" class="form-check-input">Miercuri
                                                    </label>
                                                    <label class="form-check-label ">
                                                        <input type="checkbox" id="pickup_days" name="pickup_days[]" value="Thursday" class="form-check-input">Joi
                                                    </label>
                                                    <label class="form-check-label ">
                                                        <input type="checkbox" id="pickup_days" name="pickup_days[]" value="Friday" class="form-check-input">Vineri
                                                    </label>
                                                </div>
                                            </div>
                                        </div>



                                        <div class="row form-group">
                                            <div class="col col-md-3">
                                                <label for="selectSm" class=" form-control-label">Ruta</label>
                                            </div>
                                            <div class="col-12 col-md-9">
                                                <select name="route_id" id="route_id" class="form-control-sm form-control">
                                                    @foreach ($routes as $route)
                                                        <option value="{{$route->id}}">{{$route->number}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="order" class=" form-control-label">Numar ordine.</label>
                                            <input type="number" id="order" name="order" placeholder="adauga numar ordine" class="form-control">
                                        </div>



                                            <div class="col col-md-9">
                                                <div class="col col-md-3">
                                                    <label class=" form-control-label"><strong>Alege saptamana</strong></label>
                                                </div>
                                                <div class="form-check-inline form-check">
                                                    <label class="form-check-label ">
                                                        <input type="checkbox" id="collections_rithm" name="collections_rithm[]" value=1 class="form-check-input" >1
                                                    </label>
                                                </div>
                                                <div class="form-check-inline form-check">
                                                    <label class="form-check-label ">
                                                        <input type="checkbox" id="collections_rithm" name="collections_rithm[]" value=2 class="form-check-input">2
                                                    </label>
                                                </div>
                                                <div class="form-check-inline form-check">
                                                    <label class="form-check-label ">
                                                        <input type="checkbox" id="collections_rithm" name="collections_rithm[]" value=3 class="form-check-input">3
                                                    </label>
                                                </div>
                                                <div class="form-check-inline form-check">
                                                    <label class="form-check-label ">
                                                        <input type="checkbox" id="collections_rithm" name="collections_rithm[]" value=4 class="form-check-input">4
                                                    </label>
                                                </div>
                                                <div class="form-check-inline form-check">
                                                    <label class="form-check-label ">
                                                        <input type="checkbox" id="collections_rithm" name="collections_rithm[]" value=5 class="form-check-input">5
                                                    </label>

                                                </div>


                                            </div>






                                            <div class="col col-md-9">
                                                <div class="col col-md-3">
                                                    <label class=" form-control-label"><strong>Alege cod deseuri</strong></label>
                                                </div>
                                                <div class="form-check-inline form-check">
                                                    <label class="form-check-label ">
                                                        <input type="checkbox" id="toxic_waste_code" name="toxic_waste_code[]" value=1801 class="form-check-input" >18 01
                                                    </label>
                                                </div>
                                                <div class="form-check-inline form-check">
                                                    <label class="form-check-label ">
                                                        <input type="checkbox" id="toxic_waste_code" name="toxic_waste_code[]" value=180101 class="form-check-input">18 01 01
                                                    </label>
                                                </div>
                                                <div class="form-check-inline form-check">
                                                    <label class="form-check-label ">
                                                        <input type="checkbox" id="toxic_waste_code" name="toxic_waste_code[]" value=180102 class="form-check-input">18 01 02
                                                    </label>
                                                </div>
                                                <div class="form-check-inline form-check">
                                                    <label class="form-check-label ">
                                                        <input type="checkbox" id="toxic_waste_code" name="toxic_waste_code[]" value=180103* class="form-check-input">18 01 03*
                                                    </label>
                                                </div>
                                                <div class="form-check-inline form-check">
                                                    <label class="form-check-label ">
                                                        <input type="checkbox" id="toxic_waste_code" name="toxic_waste_code[]" value=180104 class="form-check-input">18 01 04
                                                    </label>

                                                </div>
                                                <div class="form-check-inline form-check">
                                                    <label class="form-check-label ">
                                                        <input type="checkbox" id="toxic_waste_code_" name="toxic_waste_code[]" value=180106* class="form-check-input">18 01 06*
                                                    </label>

                                                </div>
                                                <div class="form-check-inline form-check">
                                                    <label class="form-check-label ">
                                                        <input type="checkbox" id="toxic_waste_code" name="toxic_waste_code[]" value=180107 class="form-check-input">18 01 07
                                                    </label>

                                                </div>
                                                <div class="form-check-inline form-check">
                                                    <label class="form-check-label ">
                                                        <input type="checkbox" id="toxic_waste_code" name="toxic_waste_code[]" value=180108* class="form-check-input">18 01 08*
                                                    </label>

                                                </div>
                                                <div class="form-check-inline form-check">
                                                    <label class="form-check-label ">
                                                        <input type="checkbox" id="toxic_waste_code" name="toxic_waste_code[]" value=180109 class="form-check-input">18 01 09
                                                    </label>

                                                </div>
                                                <div class="form-check-inline form-check">
                                                    <label class="form-check-label ">
                                                        <input type="checkbox" id="toxic_waste_code_" name="toxic_waste_code[]" value=180110* class="form-check-input">18 01 10*
                                                    </label>

                                                </div>
                                                <div class="form-check-inline form-check">
                                                    <label class="form-check-label ">
                                                        <input type="checkbox" id="toxic_waste_code" name="toxic_waste_code[]" value=1802 class="form-check-input">18 02
                                                    </label>

                                                </div>
                                                <div class="form-check-inline form-check">
                                                    <label class="form-check-label ">
                                                        <input type="checkbox" id="toxic_waste_code" name="toxic_waste_code[]" value=180201 class="form-check-input">18 02 01
                                                    </label>

                                                </div>
                                                <div class="form-check-inline form-check">
                                                    <label class="form-check-label ">
                                                        <input type="checkbox" id="toxic_waste_code" name="toxic_waste_code[]" value=180202* class="form-check-input">18 02 02*
                                                    </label>

                                                </div>
                                                <div class="form-check-inline form-check">
                                                    <label class="form-check-label ">
                                                        <input type="checkbox" id="toxic_waste_code" name="toxic_waste_code[]" value=180203 class="form-check-input">18 02 03
                                                    </label>

                                                </div>

                                                <div class="form-check-inline form-check">
                                                    <label class="form-check-label ">
                                                        <input type="checkbox" id="toxic_waste_code" name="toxic_waste_code[]" value=180205* class="form-check-input">18 02 05*
                                                    </label>

                                                </div>

                                                <div class="form-check-inline form-check">
                                                    <label class="form-check-label ">
                                                        <input type="checkbox" id="toxic_waste_code" name="toxic_waste_code[]" value=180206 class="form-check-input">18 02 06
                                                    </label>

                                                </div>

                                                <div class="form-check-inline form-check">
                                                    <label class="form-check-label ">
                                                        <input type="checkbox" id="toxic_waste_code" name="toxic_waste_code[]" value=180207* class="form-check-input">18 02 07*
                                                    </label>

                                                </div>

                                                <div class="form-check-inline form-check">
                                                    <label class="form-check-label ">
                                                        <input type="checkbox" id="toxic_waste_code" name="toxic_waste_code[]" value=180208 class="form-check-input">18 02 08
                                                    </label>

                                                </div>


                                            </div>




                                        <div class="row form-group">
                                            <div class="col col-md-3">
                                                <label class=" form-control-label">Peste o tona</label>
                                            </div>
                                            <div class="col col-md-9">
                                                <select name="overtone" id="overtone" class="form-control">
                                                    <option value="1">DA</option>
                                                    <option value="0">NU</option>
                                                </select>
                                            </div>
                                        </div>



                                        <button class="au-btn au-btn-icon au-btn--green au-btn--small">
                                            <i class="zmdi zmdi-plus"></i>adauga client
                                        </button>

                                    </div>

                                </div>
                            </div>

                        </div>
                        @include('layouts.footer')
                    </div>
                </div>
            </div>

@endsection
