


<!DOCTYPE html>
<html>
<head>
    <title>Create Drag and Droppable Datatables Using jQuery UI Sortable in Laravel</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.12/datatables.min.css"/>
    <link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"/>
</head>
<body>
    @php


    $routes = App\Route::with('clients')->get();
    $route = new App\Route;
    $week_of_month = \Illuminate\Support\Carbon::now()->weekOfMonth;
        $dt = \Illuminate\Support\Carbon::tomorrow();
        $tomorrow = '';


        if ($dt->isWeekend()) {
           $tomorrow = \Illuminate\Support\Carbon::tomorrow()->next('Monday')->format('l');
        } else {
            $tomorrow = \Illuminate\Support\Carbon::tomorrow()->format('l');
        }

        $clients = App\Client::whereJsonContains('pickup_days', [$tomorrow])
                ->whereJsonContains('collections_rithm', [(string)$week_of_month])
                ->orderBy('order', 'asc')
                ->get();



@endphp

    <div class="row mt-5">
        <div class="col-md-10 offset-md-1">
            @foreach($routes as $route)
            <h3 class="text-center mb-4">Ruta numarul {{$route->number}} </h3>
            <table id="table" class="table table-bordered">
              <thead>
                <tr>
                  <th width="30px">#</th>
                  <th>Title</th>
                  <th>Created At</th>
                </tr>
              </thead>
              <tbody class="tablecontents">

                @foreach($route->clients()->whereJsonContains('pickup_days', [$tomorrow])
                                                        ->whereJsonContains('collections_rithm', [(string)$week_of_month])
                                                        ->orderBy('order', 'asc')
                                                        ->get();
                                                        as $client)

    	            <tr class="row1" data-id="{{ $client->id }}">
    	              <td class="pl-3"><i class="fa fa-sort"></i></td>
    	              <td>{{ $client->name }}</td>
    	              <td>{{ date('d-m-Y h:m:s',strtotime($client->created_at)) }}</td>
    	            </tr>

                @endforeach
              </tbody>
            </table>
            @endforeach
            <hr>
            <h5>Drag and Drop the table rows and <button class="btn btn-success btn-sm" onclick="window.location.reload()">REFRESH</button> the page to check the Demo.</h5>
    	</div>
    </div>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.10.12/datatables.min.js"></script>
    <script type="text/javascript">
      $(function () {
        $("#table").DataTable();

        $( ".tablecontents" ).sortable({
          items: "tr",
          cursor: 'move',
          opacity: 0.6,
          update: function() {
              sendOrderToServer();
          }
        });

        function sendOrderToServer() {
          var order = [];
          var token = $('meta[name="csrf-token"]').attr('content');

          $('tr.row1').each(function(index,element) {
            order.push({
              id: $(this).attr('data-id'),
              position: index+1
            });

          });



          $.ajax({
            type: "POST",
            dataType: "json",
            url: "{{ url('/client-sortable') }}",
                data: {
              order: order,
              _token: token
            },
            success: function(response) {
                if (response.status == "success") {
                  console.log(response);
                } else {
                  console.log(response);
                }
            }
          });
        }
      });
    </script>
</body>
</html>

