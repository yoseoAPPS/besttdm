@extends('layouts.layout')

@section('content')

    <div class="main-content">
        <div class="section__content section__content--p30">
            <div class="container-fluid">
                <div class="row">
                </div>
                <section class="p-t-20">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                <h3 class="title-5 m-b-35">{{ $client->name }}</h3>
                                <div class="table-data__tool">


                                </div>
                                <div class="table-responsive table-responsive-data2">
                                    <table class="table table-data2">
                                        <thead>
                                            <tr>
                                                <th>
                                                    <label class="au-checkbox">
                                                        <input type="checkbox">
                                                        <span class="au-checkmark"></span>
                                                    </label>
                                                </th>
                                                <th>Nume</th>
                                                <th>Strada</th>
                                                <th>Localitate</th>
                                                <th>Judet</th>
                                                <th>C.U.I.</th>
                                                <th>Ziua Colectarii</th>
                                                <th>Ruta</th>
                                                <th>Peste o tona</th>
                                                <th>Ritm colectare</th>
                                                <th>Cod deseuri</th>
                                                <th>Numar ordine</th>
                                            </tr>
                                        </thead>
                                        <tbody>

                                            <tr class="tr-shadow">
                                                <td>
                                                    <label class="au-checkbox">
                                                        <input type="checkbox">
                                                        <span class="au-checkmark"></span>
                                                    </label>
                                                </td>
                                                <td>{{ $client->name }}</td>
                                                <td>
                                                    <span class="block-email">{{ $client->street }}</span>
                                                </td>
                                                <td class="desc">{{ $client->city }}</td>
                                                <td>{{ $client->county }}</td>
                                                <td>
                                                    <span class="status--process">{{ $client->cui }}</span>
                                                </td>
                                                <td>

                                                    @foreach($client->pickup_days as $pickup_day)
                                                        {{ $pickup_day }}
                                                    @endforeach

                                                </td>

                                                <td>
                                                    {{ $client->route_number }}
                                                </td>
                                                <td>{{ $client->overtone }}</td>
                                                <td>
                                                    @foreach($client->collections_rithm as $collection_rithm)
                                                        {{ $collection_rithm }}
                                                    @endforeach


                                                </td>
                                                <td>
                                                    @foreach($client->toxic_waste_code as $toxic_waste_code)
                                                        {{ $toxic_waste_code }}
                                                    @endforeach


                                                </td>
                                                <td>
                                                <td>{{ $client->order }}</td>
                                                <div class="table-data-feature">

                                                    <a href="/clients/{{ $client->id }}/edit">
                                                        <button class="item" data-toggle="tooltip" data-placement="top"
                                                            title="Edit">
                                                            <i class="zmdi zmdi-edit"></i>
                                                        </button>
                                                    </a>

                                                    <form action="/clients/{{ $client->id }}" method="POST">
                                                        @csrf
                                                        @method('DELETE')
                                                        <button class="item" data-toggle="tooltip" data-placement="top"
                                                            title="Delete">
                                                            <i class="zmdi zmdi-delete"></i>
                                                        </button>
                                                    </form>
                                                </div>
                                                </td>
                                            </tr>
                                            <tr class="spacer"></tr>


                                        </tbody>
                                    </table>


                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                @include('layouts.footer')
            </div>
        </div>
    </div>
@endsection
