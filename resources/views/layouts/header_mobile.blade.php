<header class="header-mobile d-block d-lg-none">
    <div class="header-mobile__bar">
        <div class="container-fluid">
            <div class="header-mobile-inner">

                <button class="hamburger hamburger--slider" type="button">
                    <span class="hamburger-box">
                        <span class="hamburger-inner"></span>
                    </span>
                </button>
            </div>
        </div>
    </div>
    <nav class="navbar-mobile">
        <div class="container-fluid">
            <ul class="navbar-mobile__list list-unstyled">

                <li>
                    <a href="/clients">
                        <i class="fas fa-chart-bar"></i>Clienti</a>
                </li>


                <li>
                    <a href="/drivers">
                        <i style='font-size:24px' class='fas'>&#xf0d1;</i>Soferi
                    </a>
                </li>

                <li>
                    <a href="/forms">
                        <i class="fas fa-copy"></i>Formulare</a>
                </li>


                <li class="has-sub">
                    <a class="js-arrow" href="/routes">
                        <i class='fas fa-map'></i>Rute</a>
                </li>
            </ul>
        </div>
    </nav>
</header>
