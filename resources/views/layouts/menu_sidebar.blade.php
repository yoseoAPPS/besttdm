<aside class="menu-sidebar d-none d-lg-block">
    <div class="logo">
        <a href="/">
            <img src="{{asset('images/icon/logo.png')}}" alt="Cool Admin"  style="max-width: 77px;"/>
        </a>
    </div>
    <div class="menu-sidebar__content js-scrollbar1">
        <nav class="navbar-sidebar">
            <ul class="list-unstyled navbar__list">

                <li>
                    <a href="/clients">
                        <i style='font-size:24px' class='fas'>&#xf183;</i>Clienti</a>
                </li>
                <li>
                    <a href="/drivers">
                        <i style='font-size:24px' class='fas'>&#xf0d1;</i>Soferi
                    </a>
                </li>

                <li>
                    <a href="/forms">
                        <i class="fas fa-copy"></i>Formulare</a>
                </li>


                <li class="has-sub">
                    <a class="js-arrow" href="/routes">
                        <i class='fas fa-map'></i>Rute</a>
                </li>

            </ul>
        </nav>
    </div>
</aside>
