
    ACTIVITĂŢI VETERINARE ŞI/SAU CERCETĂRI CONEXE (cu excepţia
    deşeurilor de la prepararea hranei în bucatarii sau restaurante,
    care nu au legatura directa cu activitatea sanitarã)

    18 01 deşeuri rezultate din activitãţile de prevenire, diagnostic şi
    tratament desfãşurate în unitãţile sanitare
    18 01 01 obiecte ascutite (cu excepţia 18 01 03)
    18 01 02 fragmente şi organe umane, inclusiv recipienţi de sânge şi sânge
    conservat (cu excepţia 18 01 03)
    18 01 03* deşeuri a cãror colectare şi eliminare fac obiectul unor mãsuri
    speciale privind prevenirea infectiilor
    18 01 04 deşeuri a cãror colectare şi eliminare nu fac obiectul unor mãsuri
    speciale privind prevenirea infectiilor (de ex: îmbrãcãminte,
    aparate gipsate, lenjerie, îmbrãcãminte disponibilã, scutece)
    18 01 06* chimicale constând din sau conţinând substanţe periculoase
    18 01 07 chimicale, altele decât cele specificate la 18 01 06
    18 01 08* medicamente citotoxice şi citostatice
    18 01 09 medicamente, altele decât cele specificate la 18 01 08
    18 01 10* deşeuri de amalgam de la tratamentele stomatologice
    18 02 deşeuri din unitãţile veterinare de cercetare, diagnostic, tratament
    şi prevenire a bolilor
    18 02 01 obiecte ascutite (cu excepţia 18 02 02)
    18 02 02* deşeuri a cãror colectare şi eliminare fac obiectul unor mãsuri
    speciale pentru prevenirea infectiilor
    18 02 03 deşeuri a cãror colectare şi eliminare nu fac obiectul unor mãsuri
    speciale pentru prevenirea infectiilor
    18 02 05* chimicale constând din sau conţinând substanţe periculoase
    18 02 06 chimicale, altele decât cele specificate la 18 02 05
    18 02 07* medicamente citotoxice şi citostatice
    18 02 08 medicamente, altele decât cele specificate la 18 02 07


