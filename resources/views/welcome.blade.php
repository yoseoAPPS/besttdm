@php

        $week_of_month = \Illuminate\Support\Carbon::now()->weekOfMonth;
        $dt = \Illuminate\Support\Carbon::tomorrow();
        $tomorrow = '';
        $tomorrowFull = '';
        $collections_rithm = App\Client::select('collections_rithm');


        if ($dt->isWeekend()) {
           $tomorrow = \Illuminate\Support\Carbon::tomorrow()->next('Monday')->format('l');
           $tomorrowFull = \Illuminate\Support\Carbon::tomorrow()->next('Monday')->format('d-m-Y');

        } else {
            $tomorrow = \Illuminate\Support\Carbon::tomorrow()->format('l');
            $tomorrowFull = \Illuminate\Support\Carbon::tomorrow()->format('d-m-Y');

        }



    $clients = App\Client::whereJsonContains('pickup_days', [$tomorrow])
                ->whereJsonContains('collections_rithm', [(string)$week_of_month])
                ->get();
    $drivers = App\Driver::all();


@endphp

@extends('layouts.layout')

@section('content')


        <div class="main-content">
            <div class="section__content section__content--p30">
                <div class="container-fluid">
                    
                    <div class="row m-t-25">
                        <div class="col-sm-6 col-lg-3">
                            <div class="overview-item overview-item--c1">
                                <div class="overview__inner">
                                    <div class="overview-box clearfix">
                                        <div class="icon">
                                            <i class="zmdi zmdi-account-o"></i>
                                        </div>
                                        <div class="text">

                                        <h2>{{$clients->count()}}</h2>
                                            <span>Nr. colectari</span>
                                        </div>
                                    </div>
                                    <div class="overview-chart">
                                        <canvas id="widgetChart1"></canvas>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6 col-lg-3">
                            <div class="overview-item overview-item--c2">
                                <div class="overview__inner">
                                    <div class="overview-box clearfix">
                                        <div class="icon">
                                            <i class="zmdi zmdi-truck"></i>
                                        </div>
                                        <div class="text">
                                            @php

                                            @endphp
                                            <h2>{{$drivers->count()}}</h2>
                                            <span>Soferi disponibili</span>

                                        </div>
                                    </div>
                                    <div class="overview-chart">
                                        <canvas id="widgetChart2"></canvas>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6 col-lg-3">
                            <div class="overview-item overview-item--c3">
                                <div class="overview__inner">
                                    <div class="overview-box clearfix">
                                        <div class="icon">
                                            <i class="zmdi zmdi-calendar-note"></i>
                                        </div>
                                        <div class="text">
                                        <h3>{{$tomorrowFull}}</h3>
                                        <span>Data colectare</span>
                                        </div>
                                    </div>
                                    <div class="overview-chart">
                                        <canvas id="widgetChart3"></canvas>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>



                    @include('layouts.footer')
                </div>
            </div>
        </div>
        <!-- END MAIN CONTENT-->
        <!-- END PAGE CONTAINER-->
    </div>

</div>
@endsection
