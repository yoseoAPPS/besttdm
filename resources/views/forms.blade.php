<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="content-type"	content="text/html;	charset=ISO-8859-1" />
	<title>Document</title>

	<style type="text/css">

	body {
	font-size: 11px;
	}

table {
  border-collapse: collapse;
}

table.table-row {
	border-collapse: separate;
    border-spacing: 10px 0px;
}

table.table-row tr {
	border: 0;
}


table.table-row td {
	border: 0;
}

.foreach-client table tr {
	border: solid thin black;
}

.foreach-client table td {
	border: solid thin black;
}


.foreach-client table th {
	border: solid thin black;
}


.mb-2 {
	margin-bottom: 16px;
}

table tr {
	height: 35px;
}


.page-break {
    page-break-after: always;
}



	</style>

</head>
<body>
@php


use App\Client;
$routes = App\Route::with('clients')->get();
    $week_of_month = \Illuminate\Support\Carbon::now()->weekOfMonth;
        $dt = \Illuminate\Support\Carbon::tomorrow();
        $tomorrow = '';


        if ($dt->isWeekend()) {
           $tomorrow = \Illuminate\Support\Carbon::tomorrow()->next('Monday')->format('l');
        } else {
            $tomorrow = \Illuminate\Support\Carbon::tomorrow()->format('l');
        }

        $clients = App\Client::whereJsonContains('pickup_days', [$tomorrow])
                ->whereJsonContains('collections_rithm', [(string)$week_of_month])
                ->orderBy('order', 'asc')
                ->get();

                $form_number = '|1|2|3|4|5|6|';





@endphp
	<div class="main-table">
		@foreach($routes as $route)
        @foreach($route->clients()->whereJsonContains('pickup_days', [$tomorrow])
        ->whereJsonContains('collections_rithm', [(string)$week_of_month])
        ->orderBy('order', 'asc')
        ->get();
        as $client)
	<table class="table-row">
		<tr><td>
	<div class="foreach-client left">
        <span>Anexa 2</span>
        <span>Formular de expeditie/de transport deseuri periculoase</span>
        @if($client->isOvertone())
        <p>nr. {{ $form_number }}</p>
        @endif
		<table>
			<tbody>
			<tr>
				<td>Codul deseurilor: 1801 clasa 6 UN 3291 GA II tip ambalaj Y</td>
				<td>Deseuri periculoase < 1 t/an |X|<br>
					Deseuri periculoase > 1 t/an | |
				</td>
				<td>Nr. formularului de aprobare a transportului: @if($client->isOvertone())  {{ $form_number }} @endif</td>
			</tr>
			<tr>
				<td>Nr. de inregistrare al expeditorului:</td>
				<td>Nr. de inregistrare a transportatorului</td>
				<td>Nr. de inregistrare al destinatarului:<br>
				<strong>J29/2306/2008</strong>
				</td>
			</tr>
			<tr>
				<td>In calitate de: Generator |X|<br>
					Operator economic care realizeaza operatia de: Colectare |_| Stocare temporara |_|

				</td>
				<td>In calitate de: Transportator |X|</td>
				<td>In calitate de: STOCARE TEMPORARA |_|, ELIMINARE |X|, TRATARE |_|, VALORIFICARE |_|</td>
			</tr>
			<tr>
				<td>Cantitatea predata in<br>
				Kg. |_|_|_|_|_|_|_|_|_|_|<br>
				(conform explicatiilor de mai jos)
				</td>
				<td>Cantitatea primita in<br>
				Kg. |_|_|_|_|_|_|_|_|_|_|<br>
				(conform explicatiilor)
				</td>
				<td>Cantitatea receptionata in<br>
				Kg. |_|_|_|_|_|_|_|_|_|_|<br>
				<small>(conform explicatiei de mai jos)</small><br>
				Cantitatea respinsa in<br>
				Kg. |_|_|_|_|_|_|_|_|_|_|
				</td>
			</tr>
			<tr>
            <td>Data predarii: {{$tomorrow}}<br>
				</td>
				<td>Data predarii: {{$tomorrow}}<br>
				</td>
				<td>Data primirii: {{$tomorrow}}<br>

				</td>
			</tr>
			<tr>
				<td>
                    Denumirea firmei, adresa: <br>
                    {{$client->name}}, {{$client->city}}, {{$client->county}}
                </td>
				<td>Denumirea firmei, adresa:</td>
				<td>Denumirea firmei, adresa:<br>
				SC ECO BURN SRL<br>
				RO 24333770
				</td>
			</tr>
			<tr>

				<td>Semnatura si stampila</td>
                <td>Semnatura si stampila<img src="{{asset('images/semnatura_colectare-835x.png')}}"><img src="{{asset('images/stamp_colectare-80x.png')}}"></td>
                <td>Semnatura si stampila</td>
			</tr>
			</tbody>
		</table>
		<p>Tipul mijloacelor de transport:</p>
		<p>Au fost predate spre transport deseurile:</p>
		<table class="mb-2">
			<thead>
			<tr>
			  <th>Cod deseu</th>
			  <th>Descriere deseu</th>
			  <th>Tip ambalaj</th>
			  <th>Numar ambalaje receptionate</th>
			  <th>Greutate in kg.</th>
			</tr>
			</thead>
			<tbody>
			<tr>
				<td>18 01 03</td>
				<td>Deseuri a caror colectare si eliminare fac obiectul unor masuri speciale</td>
				<td></td>
				<td></td>
                <td>5</td>
            </tr>
			<tr>
				<td>18 01 03</td>
				<td>Deseuri a caror colectare si eliminare fac obiectul unor masuri speciale</td>
				<td></td>
				<td></td>
				<td>5</td>
			</tr>
			<tr>
				<td>18 01 03</td>
				<td>Deseuri a caror colectare si eliminare fac obiectul unor masuri speciale</td>
				<td></td>
				<td></td>
				<td>5</td>
			</tr>
			<tr>
				<td>18 01 03</td>
				<td>Deseuri a caror colectare si eliminare fac obiectul unor masuri speciale</td>
				<td></td>
				<td></td>
				<td>5</td>
			</tr>
			<tr>
				<td>18 01 03</td>
				<td>Deseuri a caror colectare si eliminare fac obiectul unor masuri speciale</td>
				<td></td>
				<td></td>
				<td>5</td>
			</tr>
			<tr>
				<td>18 01 03</td>
				<td>Deseuri a caror colectare si eliminare fac obiectul unor masuri speciale</td>
				<td></td>
				<td></td>
				<td>5</td>
			</tr>
			<tr>
				<td>18 01 03</td>
				<td>Deseuri a caror colectare si eliminare fac obiectul unor masuri speciale</td>
				<td></td>
				<td></td>
				<td>5</td>
			</tr>
			<tr>
				<td>18 01 03</td>
				<td>Deseuri a caror colectare si eliminare fac obiectul unor masuri speciale</td>
				<td></td>
				<td></td>
				<td>5</td>
			</tr>
			</tbody>
		</table>
	</div>
</td>
<td>
<div class="foreach-client right">
    <span>Anexa 2</span>
        <span>Formular de expeditie/de transport deseuri periculoase</span>
        @if($client->isOvertone())
        <p>nr. {{ $form_number }}</p>
        @endif
		<table>
			<tbody>
			<tr>
				<td>Codul deseurilor: 1801 clasa 6 UN 3291 GA II tip ambalaj Y</td>
				<td>Deseuri periculoase < 1 t/an |X|<br>
					Deseuri periculoase > 1 t/an | |
				</td>
				<td>Nr. formularului de aprobare a transportului:  @if($client->isOvertone())  {{ $form_number }} @endif</td>
			</tr>
			<tr>
				<td>Nr. de inregistrare al expeditorului:</td>
				<td>Nr. de inregistrare a transportatorului</td>
				<td>Nr. de inregistrare al destinatarului:<br>
				<strong>J29/2306/2008</strong>
				</td>
			</tr>
			<tr>
				<td>In calitate de: Generator |X|<br>
					Operator economic care realizeaza operatia de: Colectare |_| Stocare temporara |_|

				</td>
				<td>In calitate de: Transportator |X|</td>
				<td>In calitate de: STOCARE TEMPORARA |_|, ELIMINARE |X|, TRATARE |_|, VALORIFICARE |_|</td>
			</tr>
			<tr>
				<td>Cantitatea predata in<br>
				Kg. |_|_|_|_|_|_|_|_|_|_|<br>
				(conform explicatiilor de mai jos)
				</td>
				<td>Cantitatea primita in<br>
				Kg. |_|_|_|_|_|_|_|_|_|_|<br>
				(conform explicatiilor)
				</td>
				<td>Cantitatea receptionata in<br>
				Kg. |_|_|_|_|_|_|_|_|_|_|<br>
				<small>(conform explicatiei de mai jos)</small><br>
				Cantitatea respinsa in<br>
				Kg. |_|_|_|_|_|_|_|_|_|_|
				</td>
			</tr>
			<tr>
				<td>Data predarii: {{$tomorrow}}<br>

				</td>
				<td>Data predarii : {{$tomorrow}}<br>

				</td>
				<td>Data primirii : {{$tomorrow}}<br>

				</td>
			</tr>
			<tr>
				<td>
                    Denumirea firmei, adresa: <br>
                    {{$client->name}}, {{$client->city}}, {{$client->county}}
                </td>
				<td>Denumirea firmei, adresa:</td>
				<td>Denumirea firmei, adresa:<br>
				SC ECO BURN SRL<br>
				RO 24333770
				</td>
			</tr>
			<tr>
				<td>Semnatura si stampila</td>
				<td></td>
				<td>Semnatura si stampila</td>
			</tr>
			</tbody>
		</table>
		<p>Tipul mijloacelor de transport:</p>
		<p>Au fost predate spre transport deseurile:</p>
		<table class="mb-2">
			<thead>
			<tr>
			  <th>Cod deseu</th>
			  <th>Descriere deseu</th>
			  <th>Tip ambalaj</th>
			  <th>Numar ambalaje receptionate</th>
			  <th>Greutate in kg.</th>
			</tr>
			</thead>
			<tbody>
			<tr>
				<td>18 01 03</td>
				<td>Deseuri a caror colectare si eliminare fac obiectul unor masuri speciale</td>
				<td></td>
				<td></td>
				<td>5</td>
			</tr>
			<tr>
				<td>18 01 03</td>
				<td>Deseuri a caror colectare si eliminare fac obiectul unor masuri speciale</td>
				<td></td>
				<td></td>
				<td>5</td>
			</tr>
			<tr>
				<td>18 01 03</td>
				<td>Deseuri a caror colectare si eliminare fac obiectul unor masuri speciale</td>
				<td></td>
				<td></td>
				<td>5</td>
			</tr>
			<tr>
				<td>18 01 03</td>
				<td>Deseuri a caror colectare si eliminare fac obiectul unor masuri speciale</td>
				<td></td>
				<td></td>
				<td>5</td>
			</tr>
			<tr>
				<td>18 01 03</td>
				<td>Deseuri a caror colectare si eliminare fac obiectul unor masuri speciale</td>
				<td></td>
				<td></td>
				<td>5</td>
			</tr>
			<tr>
				<td>18 01 03</td>
				<td>Deseuri a caror colectare si eliminare fac obiectul unor masuri speciale</td>
				<td></td>
				<td></td>
				<td>5</td>
			</tr>
			<tr>
				<td>18 01 03</td>
				<td>Deseuri a caror colectare si eliminare fac obiectul unor masuri speciale</td>
				<td></td>
				<td></td>
				<td>5</td>
			</tr>
			<tr>
				<td>18 01 03</td>
				<td>Deseuri a caror colectare si eliminare fac obiectul unor masuri speciale</td>
				<td></td>
				<td></td>
				<td>5</td>
			</tr>
			</tbody>
        </table>


	</div>
</td>
		</tr>
	</table>
<div style="page-break-after:always;"></div>
	@endforeach
	@endforeach
</div>
</body>
</html>

