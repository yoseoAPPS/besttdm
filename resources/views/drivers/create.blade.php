
@extends('layouts.layout')

@section('content')
            <!-- MAIN CONTENT-->
            <div class="main-content">
                <div class="section__content section__content--p30">
                    <div class="container-fluid">
                        <div class="row">

                            <div class="col-lg-6">
                                <div class="card">
                                    <form method="POST" action="/drivers">
                                        @csrf

                                    <div class="card-header">
                                        <strong>Adaugare transportator</strong>
                                        <small></small>
                                    </div>
                                    <div class="card-body card-block">
                                        <div class="form-group">
                                            <label for="surname" class=" form-control-label">Nume</label>
                                            <input type="text" id="surname" name="surname" placeholder="adauga nume" class="form-control">
                                        </div>
                                        <div class="form-group">
                                            <label for="firstname" class=" form-control-label">Prenume</label>
                                            <input type="text" id="firstname"  name="firstname" placeholder="adauga prenume" class="form-control">
                                        </div>
                                        <div class="form-group">
                                            <label for="plate" class=" form-control-label">Numar inmatriculare autovehicul</label>
                                            <input type="text" id="plate" name="plate" placeholder="adauga nr. inmatriculare" class="form-control">
                                        </div>

                                        <button class="au-btn au-btn-icon au-btn--green au-btn--small">
                                            <i class="zmdi zmdi-plus"></i>adauga transportator
                                        </button>

                                    </div>

                                </div>
                            </div>

                        </div>
                        @include('layouts.footer')
                    </div>
                </div>
            </div>
@endsection
