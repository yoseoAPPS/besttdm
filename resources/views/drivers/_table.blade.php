<tr class="tr-shadow row1" data-id="{{ $driver->id }}">

    <td>
        <label class="au-checkbox">
            <input type="checkbox">
            <span class="au-checkmark"></span>
        </label>
    </td>

     <td><a href="/drivers/{{$driver->id}}">{{$driver->surname}}</a></td>

    <td class="desc"><a href="/drivers/{{$driver->id}}">{{$driver->firstname}}</a></td>
    <td>{{$driver->plate}}</td>
    <td>
        <div class="table-data-feature">

            <a href="/drivers/{{$driver->id}}/edit">
                <button class="item" data-toggle="tooltip" data-placement="top" title="Edit">
                    <i class="zmdi zmdi-edit"></i>
                </button>
            </a>

            <form action="/drivers/{{$driver->id}}" method="POST">
                @csrf
                 @method('DELETE')
            <button class="item" data-toggle="tooltip" data-placement="top" title="Delete">
                <i class="zmdi zmdi-delete"></i>
            </button>
            </form>
            <button class="item" data-toggle="tooltip" data-placement="top" title="More">
                <a href="/drivers/{{$driver->id}}"><i class="zmdi zmdi-more"></i></a>
            </button>
        </div>
    </td>
</tr>
