<tr class="tr-shadow">
    <td>
        <label class="au-checkbox">
            <input type="checkbox">
            <span class="au-checkmark"></span>
        </label>
    </td>
     <td><a href="/routes/{{$route->id}}">Ruta numarul {{$route->number}}</a></td>


    <td>
        <div class="table-data-feature">

            <a href="/routes/{{$route->id}}/edit">
                <button class="item" data-toggle="tooltip" data-placement="top" title="Edit">
                    <i class="zmdi zmdi-edit"></i>
                </button>
            </a>

            <form action="/routes/{{$route->id}}" method="POST">
                @csrf
                 @method('DELETE')
            <button class="item" data-toggle="tooltip" data-placement="top" title="Delete">
                <i class="zmdi zmdi-delete"></i>
            </button>
            </form>
            <a href="/routes/{{$route->id}}">
                <button class="item" data-toggle="tooltip" data-placement="top" title="More">
                    <i class="zmdi zmdi-more"></i>
                </button>
            </a>
        </div>
    </td>
</tr>
<tr class="spacer"></tr>
