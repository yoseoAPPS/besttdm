
@extends('layouts.layout')

@section('content')
            <!-- MAIN CONTENT-->


            <div class="main-content">
                <div class="section__content section__content--p30">
                    <div class="container-fluid">
                        <div class="row">

                            <div class="col-lg-6">
                                <div class="card">
                                    <form method="POST" action="/routes">
                                        @csrf

                                    <div class="card-header">
                                        <strong>Adaugare rute</strong>
                                        <small></small>
                                    </div>
                                    @if ($errors->any())
                                        <div class="alert alert-danger">
                                            <ul>
                                                @foreach ($errors->all() as $error)
                                                    <li>{{ $error }}</li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    @endif

                                    <div class="card-body card-block">
                                        <div class="form-group">
                                            <label for="name" class=" form-control-label">Numar ruta</label>
                                            <input type="text" id="number" name="number" placeholder="adauga numar ruta" class="form-control">
                                        </div>




                                        <button class="au-btn au-btn-icon au-btn--green au-btn--small">
                                            <i class="zmdi zmdi-plus"></i>adauga ruta
                                        </button>

                                    </div>

                                </div>
                            </div>

                        </div>
                        @include('layouts.footer')
                    </div>
                </div>
            </div>
@endsection
