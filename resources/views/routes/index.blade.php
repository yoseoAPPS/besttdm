@extends('layouts.layout')

@section('content')




    <div class="main-content">
        <div class="section__content section__content--p30">
            <div class="container-fluid">
                <div class="row">
                </div>
                    <section class="p-t-20">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-12">
                                    <h3 class="title-5 m-b-35">Rute</h3>

                                    <div class="table-data__tool">

                                        <div class="table-data__tool-right">
                                            <a href="/routes/create">
                                                <button class="au-btn au-btn-icon au-btn--green au-btn--small">
                                                <i class="zmdi zmdi-plus"></i>adauga rute</button>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="table-responsive table-responsive-data2">
                                        <table class="table table-data2">
                                            <thead >
                                                <tr>
                                                    <th>
                                                        <label class="au-checkbox">
                                                            <input type="checkbox">
                                                            <span class="au-checkmark"></span>
                                                        </label>
                                                    </th>
                                                    <th>Numar ruta</th>
                                                </tr>
                                            </thead>
                                            <tbody>



                                                @foreach($routes as $route)

                                                @include('routes._table')

                                                @endforeach

                                            </tbody>
                                        </table>


                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                    @include('layouts.footer')
            </div>
        </div>
    </div>
@endsection
