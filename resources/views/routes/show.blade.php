@extends('layouts.layout')

@section('content')

    <div class="main-content">
        <div class="section__content section__content--p30">
            <div class="container-fluid">
                <div class="row">
                </div>
                    <section class="p-t-20">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-12">
                                    <h3 class="title-5 m-b-35">Ruta numarul {{$route->number}}</h3>
                                    <div class="table-data__tool">


                                    </div>

                                    <div class="table-responsive table-responsive-data2">
                                        @foreach($route->clients as $client)
                                        <table class="table table-data2">

                                            <thead>
                                                <tr>
                                                    <th>
                                                        <label class="au-checkbox">
                                                            <input type="checkbox">
                                                            <span class="au-checkmark"></span>
                                                        </label>
                                                    </th>
                                                    <th>Nume</th>
                                                    <th>Localitate</th>
                                                    <th>Judet</th>
                                                    <th>Ritm colectare</th>
                                                    <th>Ziua Colectarii</th>
                                                </tr>
                                            </thead>

                                            <tbody>


                                                <tr class="tr-shadow">
                                                    <td>
                                                        <label class="au-checkbox">
                                                            <input type="checkbox">
                                                            <span class="au-checkmark"></span>
                                                        </label>
                                                    </td>
                                                    <td>{{$client->name}}</td>

                                                    <td class="desc">{{$client->city}}</td>
                                                    <td>{{$client->county}}</td>
                                                    <td>
                                                        @foreach($client->collections_rithm as $collection_rithm)
                                                        {{$collection_rithm}}
                                                    @endforeach

                                                    </td>


                                                    <td>

                                                            @foreach($client->pickup_days as $pickup_day)
                                                                {{$pickup_day}}
                                                            @endforeach
                                                    </td>





                                                        <div class="table-data-feature">

                                                            <a href="/clients/{{$client->id}}/edit">
                                                                <button class="item" data-toggle="tooltip" data-placement="top" title="Edit">
                                                                    <i class="zmdi zmdi-edit"></i>
                                                                </button>
                                                            </a>

                                                            <form action="/clients/{{$client->id}}" method="POST">
                                                                @csrf
                                                                 @method('DELETE')
                                                            <button class="item" data-toggle="tooltip" data-placement="top" title="Delete">
                                                                <i class="zmdi zmdi-delete"></i>
                                                            </button>
                                                            </form>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr class="spacer"></tr>



                                            </tbody>


                                        </table>
                                        @endforeach


                                    </div>

                                </div>
                            </div>
                        </div>
                    </section>
                    @include('layouts.footer')
            </div>
        </div>
    </div>
@endsection
