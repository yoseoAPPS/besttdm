<?php

use App\Http\Controllers\ClientsController;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::view('/', 'welcome');


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/clients', 'ClientsController@index');
Route::post('/clients', 'ClientsController@store');
Route::get('/clients/create', 'ClientsController@create');
Route::get('/clients/{client}', 'ClientsController@show');
Route::get('/clients/{client}/edit', 'ClientsController@edit');
Route::put('/clients/{client}', 'ClientsController@update');
Route::delete('/clients/{client}', 'ClientsController@destroy');

Route::get('/forms', 'ClientsController@formsindex');


Route::post('client-sortable','ClientsController@reorder');



Route::get('/drivers', 'DriversController@index');
Route::post('/drivers', 'DriversController@store');
Route::get('/drivers/create', 'DriversController@create');
Route::get('/drivers/{driver}', 'DriversController@show');
Route::get('/drivers/{driver}/edit', 'DriversController@edit');
Route::put('/drivers/{driver}', 'DriversController@update');
Route::delete('/drivers/{driver}', 'DriversController@destroy');

Route::get('/routes', 'RouteController@index');
Route::post('/routes', 'RouteController@store');
Route::get('/routes/create', 'RouteController@create');
Route::get('/routes/{route}', 'RouteController@show');
Route::get('/routes/{route}/edit', 'RouteController@edit');
Route::put('/routes/{route}', 'RouteController@update');
Route::delete('/routes/{route}', 'RouteController@destroy');




Route::get('importExportView', 'ImpExpController@importExportView');
Route::get('export', 'ImpExpController@export')->name('export');
Route::post('import', 'ImpExpController@import')->name('import');



//PDF
Route::get('generate-pdf','PDFController@generatePDF');


//MAIL

Route::get('form-mail','MailController@FormMail');


//REORDER
//REORDER
Route::get('post','MenuController@index');
Route::post('post-sortable','MenuController@updateorder');

