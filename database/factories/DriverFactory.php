<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;
use App\Driver;


$factory->define(Driver::class, function (Faker $faker) {
    return [
        'surname' => $faker->sentence(20),
        'firstname' => $faker->sentence(20),
        'plate' => $faker->sentence(20)
    ];
});
