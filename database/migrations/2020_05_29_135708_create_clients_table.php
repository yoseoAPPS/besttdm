<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clients', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->text('name');
            $table->text('city');
            $table->text('county');
            $table->text('street');
            $table->integer('cui');
            $table->json('collections_rithm');
            $table->json('pickup_days');
            $table->integer('route_number');
            $table->text('overtone');
            $table->json('toxic_waste_code');
            $table->integer('order_number');




        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       Schema::dropIfExists('clients');
    }
}
