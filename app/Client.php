<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    protected $fillable = [
        'name', 'street', 'city','county', 'cui', 'pickup_days', 'overtone', 'route_id', 'collections_rithm', 'toxic_waste_code', 'order'

    ];

    protected $casts = [
        'pickup_days' => 'array',
        'collections_rithm' =>'array',
        'toxic_waste_code'=>'array',

    ];

    public function route(){
        return $this->belongsTo(Route::class);
    }

    public function isOvertone()
    {
        return $this->overtone == '1';

    }












}
