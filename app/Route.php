<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Route extends Model
{
    protected $fillable = ['number'];

    public function clients()
    {
        return $this->hasMany(Client::class);
    }

    protected $casts = [
        'pickup_days' => 'array',
        'collections_rithm' =>'array',
        'toxic_waste_code'=>'array',

    ];
}
