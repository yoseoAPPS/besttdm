<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Barryvdh\DomPDF\Facade as PDF;
use App\Client;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Mail;
use App\Route;

class PDFController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function generatePDF()
    {

        $routes = Route::with('clients')->get();

    $week_of_month = Carbon::now()->weekOfMonth;
        $dt =Carbon::tomorrow();
        $tomorrow = '';


        if ($dt->isWeekend()) {
           $tomorrow = \Illuminate\Support\Carbon::tomorrow()->next('Monday')->format('l');
        } else {
            $tomorrow = \Illuminate\Support\Carbon::tomorrow()->format('l');
        }

        $week_of_month =Carbon::now()->weekOfMonth;
            $dt = Carbon::tomorrow();
            $tomorrow = '';



            $clients = Client::whereJsonContains('pickup_days', [$tomorrow])
                ->whereJsonContains('collections_rithm', [(string)$week_of_month])
                ->orderBy('order', 'asc')
                ->get();

        /*$data = ['title' => 'Welcome to ItSolutionStuff.com'];
        $pdf = PDF::loadView('myPDF', $data);

        return $pdf->download('itsolutionstuff.pdf');

        */


        //$path = Storage::disk('local')->put('forms.pdf', 'Contents');
        $path = storage_path('app/forms.pdf');
        $data = [ 'name' => 'required',
        'street' => 'nullable',
        'city'=> 'required',
        'county' => 'required',
        'cui'=> 'required',
        'overtone'=>'required',
        'route_number'=>'required',
        'collections_rithm'=>'nullable',
        'toxic_waste_code'=>'nullable',
        'pickup_days'=>'required|min:1'];

        $pdf = PDF::loadView('forms', $clients)->setPaper('a4', 'landscape');
        return $pdf->download('forms.pdf');






        //->setPaper('a4', 'landscape');


       // return $pdf->download('forms.pdf');

    }
}
