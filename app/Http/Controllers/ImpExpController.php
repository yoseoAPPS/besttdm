<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use App\Exports\ClientsExport;
use App\Imports\CLientsImport;
use Maatwebsite\Excel\Facades\Excel;

class ImpExpController extends Controller
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function importExportView()
    {
       return view('import');
    }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function export()
    {
        return Excel::download(new ClientsExport, 'clients.xlsx');
    }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function import()
    {
        Excel::import(new ClientsImport,request()->file('file'));

        return back();
    }
}
