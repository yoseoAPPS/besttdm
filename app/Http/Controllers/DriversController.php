<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Driver;

class DriversController extends Controller
{
    public function index(){
        $drivers = Driver::paginate(5);
         return view('drivers.index', [
             'drivers' => $drivers,
         ]);
     }

     public function store(Driver $driver)
     {
         Driver::create([
             'surname' => request('surname'),
             'firstname' => request('firstname'),
             'plate'=> request('plate')
         ]);
         return redirect('/drivers');
     }


     public function create(Driver $driver)
     {
         return view('drivers.create', ['driver'=>$driver]);

     }



     public function show($id){
         $driver = Driver::find($id);

         return view('drivers.show', [
             'driver'=>$driver
             ]);
     }

     public function edit($id){
         $driver = Driver::find($id);
         return view('drivers.edit', compact('driver'));
     }

     public function update($id){
         $driver= Driver::find($id);
         $driver->surname=request('surname');
         $driver->firstname=request('firstname');
         $driver->plate=request('plate');

         $driver->save();
         return redirect('/drivers');
     }

     public function destroy($id){
         $driver = Driver::find($id);
         $driver->delete($id);
         return redirect('/drivers');

     }
}
