<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use App\Route;
use App\Client;

class RouteController extends Controller
{
    public function index(){
        $clients=Client::all();

        $routes = Route::with('clients')->get();

        return view('routes.index', ['routes'=>$routes]);


    }


    public function store(Request $request)
    {
        $route= new Route();
        $route->number=request('number');

        $route->save();
        return redirect('/routes');
    }


    public function create(Route $route)
    {
        return view('routes.create', ['route'=>$route]);

    }



    public function show($id){
        $clients = Client::all();

        $route = Route::find($id);
        $client = Client::find($id);

        return view('routes.show', [
            'route'=>$route,
            'client'=>$client

            ]);
    }

    public function edit($id){
        $route = Route::find($id);
        return view('routes.edit', compact('route'));
    }

    public function update($id, Request $request){
        $route= Route::find($id);
        $route->number=request('number');


        $route->save();
        return redirect('/routes');
    }

    public function destroy($id){
        $route = Route::find($id);
        $route->delete($id);
        return redirect('/routes');

    }
}
