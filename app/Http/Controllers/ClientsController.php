<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Client;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Carbon;
use Illuminate\Auth\Events\Validated;
use App\Driver;
use App\Route;

class ClientsController extends Controller


{
    public function index()
    {

        $routes = Route::with('clients')->get();

        $week_of_month = Carbon::now()->weekOfMonth;
        $dt = Carbon::tomorrow();
        $tomorrow = '';


        if ($dt->isWeekend()) {
            $tomorrow = Carbon::tomorrow()->next('Monday')->format('l');
        } else {
            $tomorrow = Carbon::tomorrow()->format('l');
        }

        $clients = Client::whereJsonContains('pickup_days', [$tomorrow])
            ->whereJsonContains('collections_rithm', [(string)$week_of_month])
            ->orderBy('order', 'asc')
            ->get();


        return view('clients.index');
    }


    public function store(Request $request)
    {
        $client = new Client();
        $client->name = request('name');
        $client->street = request('street');
        $client->city = request('city');
        $client->county = request('county');
        $client->cui = request('cui');
        $client->overtone = request('overtone');
        $client->collections_rithm = request('collections_rithm');
        $client->pickup_days = request('pickup_days');
        $client->toxic_waste_code = request('toxic_waste_code');
        // $client->order_number=request('order_number');
        $client->order = request('order');

        $client->route_id = request('route_id');




        $validatedData = $request->validate([

            'name' => 'required',
            'street' => 'nullable',
            'city' => 'required',
            'county' => 'required',
            'cui' => 'required',
            'overtone' => 'required',
            'collections_rithm' => 'nullable',
            'toxic_waste_code' => 'required',
            'pickup_days' => 'required|min:1',
            'order' => 'required'
        ]);







        $client->save();

        return redirect('/clients');
    }


    public function create(Client $client)
    {
        $routes = Route::all();
        return view('clients.create', ['client' => $client]);
    }



    public function show($id)
    {
        $client = Client::find($id);
        $route = Route::find($id);

        return view('clients.show', [
            'client' => $client,
            'route' => $route
        ]);
    }

    public function edit($id)
    {
        $client = Client::find($id);
        $routes = Route::all();

        $week_of_month = Carbon::now()->weekOfMonth;
        $dt = Carbon::tomorrow();
        $tomorrow = '';


        if ($dt->isWeekend()) {
            $tomorrow = Carbon::tomorrow()->next('Monday')->format('l');
        } else {
            $tomorrow = Carbon::tomorrow()->format('l');
        }

        $clients = Client::whereJsonContains('pickup_days', [$tomorrow])
            ->whereJsonContains('collections_rithm', [(string)$week_of_month])
            ->orderBy('order', 'asc')
            ->get();





        return view('clients.edit', compact('client', 'clients'));
    }

    public function update($id, Request $request)
    {
        $client = Client::find($id);
        $client->name = request('name');
        $client->street = request('street');
        $client->city = request('city');
        $client->county = request('county');
        $client->cui = request('cui');
        $client->pickup_days = request('pickup_days');
        $client->overtone = request('overtone');
        $client->collections_rithm = request('collections_rithm');
        $client->toxic_waste_code = request('toxic_waste_code');
        // $client->order_number=request('order_number');
        $client->order = request('order');

        $client->route_id = request('route_id');



        $client->save();
        return redirect('/clients');
    }

    public function destroy($id)
    {
        $client = Client::find($id);
        $client->delete($id);
        return redirect('/clients');
    }


    public function reorder(Request $request)
    {
        $clients = Client::all();

        foreach ($clients as $client) {
            foreach ($request->order as $order) {
                if ($order['id'] == $client->id) {
                    $client->update(['order' => $order['position']]);
                }
            }
        }

        return response('Update Successfully.', 200);
    }


    public function formsindex()
    {
        $routes = Route::with('clients')->get();
        $route = new Route;
        $week_of_month = Carbon::now()->weekOfMonth;
        $dt = Carbon::tomorrow();
        $tomorrow = '';
        $drivers = Driver::all();
        $tomorrowFull = Carbon::tomorrow()->format('d-m-Y');


        if ($dt->isWeekend()) {
            $tomorrow = Carbon::tomorrow()->next('Monday')->format('l');
        } else {
            $tomorrow = Carbon::tomorrow()->format('l');
        }


        $clients = Client::whereJsonContains('pickup_days', [$tomorrow])
            ->whereJsonContains('collections_rithm', [(string)$week_of_month])
            ->orderBy('order', 'asc')
            ->get();

        $client = new Client;

        $form_number = '|1|2|3|4|5|6|';



        return view('forms', [

            'form_number' => $form_number,
            'clients' => $clients,
            'routes' => $routes,
            'route' => $route,
            'client' => $client,
            'week_of_month' => $week_of_month,
            'dt' => $dt,
            'tomorrow' => $tomorrow,
            'tomorrowFull' => $tomorrowFull
        ]);
    }
}
