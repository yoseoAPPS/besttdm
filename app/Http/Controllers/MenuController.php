<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Menu;

class MenuController extends Controller
{
    public function index(Request $request){
        $posts = Menu::orderBy('lable','ASC')->get();

        return view('post',compact('posts'));
    }

    public function updateorder(Request $request){
        $posts = Menu::all();

        foreach ($posts as $post) {
            foreach ($request->label as $order) {
                if ($order['id'] == $post->id) {
                    $post->update(['label' => $order['position']]);
                }
            }
        }

        return response('Update Successfully.', 200);
    }
}
