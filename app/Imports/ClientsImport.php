<?php

namespace App\Imports;

use App\Client;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;


class ClientsImport implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new Client([
            'name' => $row['name'],
            'street' => $row['street'],
            'city'=> $row['city'],
            'county' => $row['county'],
            'cui'=> $row['cui'],
            'overtone'=> $row['overtone'],
            'route_number'=> $row['route_number'],
            'collections_rithm'=> $row['collections_rithm'],
            'pickup_days'=> $row['pickup_days'],
        ]);
    }
}
