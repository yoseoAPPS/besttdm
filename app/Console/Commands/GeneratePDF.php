<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Client;
use Illuminate\Support\Carbon;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Support\Facades\Storage;
use App\Route;
class GeneratePDF extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'generate:pdf';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate PDF from blade view';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $routes = Route::with('clients')->get();

    $week_of_month = Carbon::now()->weekOfMonth;
        $dt =Carbon::tomorrow();
        $tomorrow = '';


        if ($dt->isWeekend()) {
           $tomorrow = Carbon::tomorrow()->next('Monday')->format('l');
        } else {
            $tomorrow = Carbon::tomorrow()->format('l');
        }

        $week_of_month =Carbon::now()->weekOfMonth;
            $dt = Carbon::tomorrow();




            $clients = Client::whereJsonContains('pickup_days', [$tomorrow])
                ->whereJsonContains('collections_rithm', [(string)$week_of_month])
                ->orderBy('order', 'asc')
                ->get();
        $form_number = '|1|2|3|4|5|6|';
        $path = storage_path('app/public')."forms.pdf";
        $tomorrowFull = Carbon::tomorrow()->format('d-m-Y');

        $pdf = PDF::loadView('forms', ['clients'=>$clients,
        'tomorrowFull'=>$tomorrowFull,
        'form_number'=>$form_number])->setPaper('a4', 'landscape');
        $pdf->save($path);


        return $path;

    }
}

