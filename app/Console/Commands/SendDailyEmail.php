<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Client;
use Illuminate\Support\Carbon;
use CS_REST_Campaigns;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;
use App\Route;


class SendDailyEmail extends Command
{

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'notify:email';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send the daily email';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {


        $routes = Route::with('clients')->get();

        $week_of_month = Carbon::now()->weekOfMonth;
            $dt =Carbon::tomorrow();
            $tomorrow = '';


            if ($dt->isWeekend()) {
               $tomorrow =Carbon::tomorrow()->next('Monday')->format('l');
            } else {
                $tomorrow =Carbon::tomorrow()->format('l');
            }

            $week_of_month =Carbon::now()->weekOfMonth;
                $dt = Carbon::tomorrow();
                $tomorrow = '';
                $tomorrowFull = Carbon::tomorrow()->format('d-m-Y');




                $clients = Client::whereJsonContains('pickup_days', [$tomorrow])
                    ->whereJsonContains('collections_rithm', [(string)$week_of_month])
                    ->orderBy('order', 'asc')
                    ->get();




        Mail::send(['html'=>'forms'], array('clients'=>$clients), function($message)
        {
            $path = storage_path('app/public')."forms.pdf";

            $message->from('dumitrumarian2503@gmail.com');
            $message->subject('anexa');
            $message->to('saw45862tttup3@print.epsonconnect.com');
            $message->attach($path);

        }


    );
    }




}

